Monitor Firewall Resources
==========================

At least python version 3.2 is needed.

Purpose
-------
PAN-OS doesn’t provide an automated mechanism to historically trend firewall resources over time. Although CLI commands provide this text output in the command line, users must manually execute these commands and track the results independently. This makes historical trending of these resources difficult.

Solution
--------
To automate the historical trending of CPU, packet buffer utilization, session count, CPS, PPS, and throughput, a Python script can be used to monitor the firewall’s usage automatically and save the results to an output log file.  The script uses API commands to extract the firewall’s metrics using the "show running resource-monitor second last 1" and "show session info" at a given interval defined by the user to build the historical view.

Upon termination of the script, a log file is produced within the local directory that the script is saved in. At the end of the output file are average and maximum statistics for all measured values over the polling interval over all iterations.


Script Parameters
-----------------
```
python monitor_fw_stats.py -h

usage: monitor_fw_stats.py -f <ip-address> [-k <key> | -u <username> -p <password>] [-i <interval>] [-n <#iterations>] [-csv?h]

    -f <ip-address>   Firewall IP-Address
    -k <key>          Use supplied key for api interaction, key is not generated
    -u <username>     Username to use for firewall key (default: admin)
    -p <password>     Password to use for firewall key (default: interactively ask for a password)
    -i <interval>     Interval in seconds between fetching firewall statistics (default=10)
    -n <#iterations>  Number of iterations to fetch firewall statistics
                      (if not defined or 0 it will iterate until Enter / Return is pressed)
    -c                Prints comma separated format with leading date/time column
                      instead of human readable for incorporating in sheets for graphing
    -s                Silent, no info on stdout
    -d                Show data also on stdout while executing, -s overrides this setting
    -v                Show version
    -?, -h            This message

```

Script Termination
------------------
Once the correct input is entered and the script is successfully running, it will collect the given number of iterations of statistics collection or run indefinitely. At any point in time, the user can press Enter/Return to terminate the script and generate the corresponding log file (```monitor_fw_usageyyyymmddhhss....```) in either screen (```.txt```) or csv (```.csv```) format.

The csv-output can be used for graphing in spreadsheets. 
