import getopt
import logging
import os
import re
import requests
import sys
import time
from getpass import getpass
from select import select
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)



# Altered for use on Mac, removed msvcrt dependency, added iterations
# Added more options to use username/password as arguments
# Added csv format for easy import
# Added progress indicator
# Added silent option
# Added asking for password
# Added supplying already known api-key
# Added ask pwd, keygen once

# adopted option to use Enter/Return to stop iterating using select IO completion
# adopted argparse, introduced in python version 3.2 so watch out for possible issues
# Didn't look at portability


versionmsg = __file__ + " version 3.7.r30"
usagemsg = """

usage: """ + __file__ + """ -f <ip-address> [-k <key> | -u <username> -p <password>] [-i <interval>] [-n <#iterations>] [-csv?h]

    -f <ip-address>   Firewall IP-Address
    -k <key>          Use supplied key for api interaction, key is not generated
    -u <username>     Username to use for firewall key (default: admin)
    -p <password>     Password to use for firewall key (default: interactively ask for a password)
    -i <interval>     Interval in seconds between fetching firewall statistics (default=10)
    -n <#iterations>  Number of iterations to fetch firewall statistics
                      (if not defined or 0 it will iterate until Enter / Return is pressed)
    -c                Prints comma separated format with leading date/time column
                      instead of human readable for incorporating in sheets for graphing
    -s                Silent, no info on stdout
    -d                Show data also on stdout while executing, -s overrides this setting
    -v                Show version
    -?, -h            This message

    """


def key_grab(IP,user,password):
    """Take in input IP, uname and password and retrieve API key from firewall"""
    logging.debug("In API key gen routine")
    try:
        resp = requests.get('https://{0}/api?type=keygen&user={1}&password={2}'.format(IP, user, password), verify=False, timeout = 240)
    except IOError:
        print ("Unable to get the device's API key as the connection was refused/timed out. Please check connectivity.")
        logging.debug("Error occurred when trying to retrieve API key")
        raise SystemExit(1)
    data1 = resp.text
    keystart = data1.find("<key>")
    if keystart < 0 :
        print ("Unable to get key from firewall, returned data: ", data1 )
        print (usagemsg)
        raise SystemExit(1)
    keystart = data1.find(">", keystart) + 1
    keyend = data1.find("</key>", keystart)
    api_key = data1[keystart:keyend]
    return api_key

def showSessionInfo(data,keyword):
    startidx = data.find(keyword)
    startidx = data.find(">", startidx) + 1
    endidx = data.find ("</", startidx)
    value = data[startidx:endidx]
    return value

def main(argv):
    IP = ""
    interval = 10
    iterations = 0
    n = 0
    csv = False
    silent = False
    showdata = False
    username = "admin"
    password = ""
    key = ""
    bArray = []
    btempArray = []
    cArray = []
    ctempArray = []
    numMaxArray = []
    numActiveArray = []
    cpsArray = []
    ppsArray = []
    kbpsArray = []
    timeArray = []
    averageStats = [0,0,0,0,0,0]
    maxStats = [0,0,0,0,0,0]

    ## Checks command line arguments for correct inputs
    ## Is checked by checking the args
    ##if len(sys.argv) != 5:
    ##    print (usagemsg)
    ##    sys.exit(1)
    try:
        opts, args = getopt.getopt(argv,"f:i:n:u:p:k:?hcsdv",["ffile=","ifile=","numberofiterations=","username=","password=","key=","help","csv","silent","showdata","version"])
    except getopt.GetoptError:
        print (usagemsg)
        sys.exit(1)
    for opt, arg in opts:
        if opt in ("-f", "--ffile"):
            IP = arg
        elif opt in ("-i", "--ifile"):
            interval = arg
        elif opt in ("-n", "--numberofiterations"):
            iterations = int(arg)
        elif opt in ("-u", "--username"):
            username = arg
        elif opt in ("-p", "--password"):
            password = arg
        elif opt in ("-k", "--key"):
            key = arg
        elif opt in ("-c", "--csv"):
            csv = True
        elif opt in ("-s", "--silent"):
            silent = True
        elif opt in ("-d", "--showdata"):
            showdata = True
        elif opt in ("-v", "--version"):
            print (versionmsg)
            sys.exit(1)
        elif opt in ("-?", "-h", "--help"):
            print (usagemsg)
            sys.exit(1)

    ## IP is needed
    if ( IP == "" ) :
        print ("ERROR - IP address must be supplied")
        print (usagemsg)
        sys.exit(1)

    ## Ensures interval is valid
    interval = int(interval)
    if(interval < 1):
        print ("ERROR - Polling Interval Must Be At Least 1")
        print (usagemsg)
        sys.exit(1)

    # Interactively get the password from input if not in args
    # or (backwards compatibility) if "ask" in args, impossible to be a real PAN-OS password. :)
    # Generates key, if not supplied, for use with the firewall
    if key == "" :
        if password == "" or password == "ask" :
            password = getpass()
        key = key_grab(IP,username,password)

    if silent :
        showdata = False

    if not silent :
        print("IP: ", IP)
        print("Interval: ", interval)
        print("Iterations: ", iterations)
        print("Username: ", username)
        print("Password: ********")
        print("Key: ", key)
        print("csv: ", csv)
        print("Silent: ", silent)
        print("*** Press Enter/Return to Stop Script ***")

    if showdata :
        print("Packet Buffers        Avg CPU        Allocated Sessions         CPS         PPS         kbps")

    ## Adds start time to output log file
    outputFileName = "monitor_fw_usage" + time.strftime("%Y%m%d") + time.strftime("%I%M%S")
    if csv :
        outputFileName += ".csv"
    else :
        outputFileName += ".txt"
    file = open(outputFileName, "w")
    stime = ("Start Time: " + time.strftime("%d/%m/%Y ") + time.strftime("%I:%M:%S"))
    file.write(stime + "\n")
    file.close()

    while 1:
        resp = requests.get('https://{0}/api/?type=op&cmd=op&cmd=<show><running><resource-monitor><second><last>1<%2Flast><%2Fsecond><%2Fresource-monitor><%2Frunning><%2Fshow>&key={1}'.format(IP, key), verify=False)
        data = resp.text

        if data.find("error") > 0 :
            startidx = data.find("<msg>")+5
            endidx = data.find("</msg>")
            if startidx < 0 or endidx < 0 :
                print ( "\n\nError fetching data: ", data )
            else :
                print ( "\n\nError fetching data: ",data[startidx:endidx] )
            sys.exit(1)
 
        ## Finds buffer values and averages upon all DPs
        bufferval = data.find("buffer")
        while(bufferval != -1):
            startidx = data.find("value", bufferval)
            startidx = data.find(">", startidx) + 1 ## Goes to the end of <value>
            endidx = data.find("</value>", startidx)
            bvalue = int(data[startidx:endidx])
            btempArray.append(bvalue)
            endidx = data.find(">", endidx) + 1 ## Goes to the end of </value>
            bufferval = data.find("buffer", endidx)

        ## Finds CPU values and averages upon all DPs
        cpuval = data.find("flow_lookup")
        dpcount = 0
        while(cpuval != -1):
            dpcount += 1
            cpustart = data.find(">", cpuval) + 1 ## Goes to the end of <flow_lookup>
            cpuend = data.find("%", cpustart)
            cvalue = int(data[cpustart:cpuend])
            ctempArray.append(cvalue)
            cpuend = data.find(">", cpuend) + 1 ## Goes to the end of </flow_lookup>
            cpuval = data.find("flow_lookup", cpuend)

        ## Averages values upon all DPs
        k = 0
        buffertotal = 0
        cputotal = 0
        while(k < len(ctempArray)):
            buffertotal += btempArray[k]
            cputotal += ctempArray[k]
            k += 1
        buffertotal = buffertotal / dpcount
        cputotal = cputotal / dpcount
        bArray.append(buffertotal)
        cArray.append(cputotal)

        btempArray = []
        ctempArray = []

        ## show session info
        resp2 = requests.get('https://{0}/api/?type=op&cmd=op&cmd=<show><session><info><%2Finfo><%2Fsession><%2Fshow>&key={1}'.format(IP, key), verify=False)
        data2 = resp2.text

        numActiveVal = showSessionInfo(data2,"num-active")
        cpsVal = showSessionInfo(data2,"cps")
        ppsVal = showSessionInfo(data2,"pps")
        kbpsVal = showSessionInfo(data2,"kbps")
        timeVal = time.strftime("%d/%m/%Y ") + time.strftime("%I:%M:%S")

        numActiveArray.append(numActiveVal)
        cpsArray.append(cpsVal)
        ppsArray.append(ppsVal)
        kbpsArray.append(kbpsVal)
        timeArray.append(timeVal)

        averageStats[0] += int(buffertotal)
        averageStats[1] += int(cputotal)
        averageStats[2] += int(numActiveVal)
        averageStats[3] += int(cpsVal)
        averageStats[4] += int(ppsVal)
        averageStats[5] += int(kbpsVal)

        if int(buffertotal) > maxStats[0] :
            maxStats[0] = int(buffertotal)
        if int(cputotal) > maxStats[1] :
            maxStats[1] = int(cputotal)
        if int(numActiveVal) > maxStats[2] :
            maxStats[2] = int(numActiveVal)
        if int(cpsVal) > maxStats[3] :
            maxStats[3] = int(cpsVal)
        if int(ppsVal) > maxStats[4] :
            maxStats[4] = int(ppsVal)
        if int(kbpsVal) > maxStats[5] :
            maxStats[5] = int(kbpsVal)

        # iterate...
        if iterations > 0 :
            n += 1

        # show progress
        if not silent and not showdata :
            if iterations == 0 :
                sys.stdout.write("\r%s" % timeVal)
            else :
                sys.stdout.write("\r%d%%" % int(n/iterations*100))
            sys.stdout.flush()
        elif showdata :
            print (str(bArray[-1]) + "%            " + str(cArray[-1]) + "%        " + str(numActiveArray[-1]) + "                " + str(cpsArray[-1]) + "        " + str(ppsArray[-1]) + "         " + str(kbpsArray[-1]))

        ## Ends program after n times or if Enter/Return pressed
        if iterations > 0 :
            if n >= iterations :
                break
        rlist, wlist, xlist = select([sys.stdin], [], [], interval)
        if rlist :
            break

    ## End while loop

    if not silent and not showdata :
        if iterations > 0 :
            sys.stdout.write("\r100%, Finished.\n")
        else :
            sys.stdout.write("\rFinished.\n")
        sys.stdout.flush()
        print("Generating Log File...")

    with open(outputFileName, "a") as file:
        if csv :
            file.write("Date/Time,Packet Buffers,Avg CPU,Allocated Sessions,CPS,PPS,kbps")
        else :
            file.write("Packet Buffers        Avg CPU        Allocated Sessions         CPS         PPS         kbps")
        file.write("\n")

        i = 0
        while(i < len(bArray)):
            if csv :
                file.write(str(timeArray[i]) + "," + str(bArray[i]) + "%," + str(cArray[i]) + "%," + str(numActiveArray[i]) + "," + str(cpsArray[i]) + "," + str(ppsArray[i]) + "," + str(kbpsArray[i]) + "\n")
            else :
                file.write(str(bArray[i]) + "%            " + str(cArray[i]) + "%        " + str(numActiveArray[i]) + "                " + str(cpsArray[i]) + "        " + str(ppsArray[i]) + "         " + str(kbpsArray[i]) + "\n")
            i += 1
        file.write("End Time: " + time.strftime("%d/%m/%Y ") + time.strftime("%I:%M:%S") + "\n")
        file.write("Average/Max Stats\n")

        i = 0
        while(i < len(averageStats)):
            averageStats[i] = round((averageStats[i] / len(bArray)),0)
            maxStats[i] = round((maxStats[i]),0)
            i += 1

        file.write("Metric:         average / maximum values" + "%\n")
        file.write("Packet Buffers: " + str(averageStats[0]) + "% / "+ str(maxStats[0]) + "%\n")
        file.write("CPU:            " + str(averageStats[1]) + "% / "+ str(maxStats[1]) + "%\n")
        file.write("Sessions:       " + str(averageStats[2]) + " / "+ str(maxStats[2]) + "\n")
        file.write("CPS:            " + str(averageStats[3]) + " / "+ str(maxStats[3]) + "\n")
        file.write("PPS:            " + str(averageStats[4]) + " / "+ str(maxStats[4]) + "\n")
        file.write("Kbps:           " + str(averageStats[5]) + " / "+ str(maxStats[5]) + "\n")

        if not silent :
            print("Metric:         average / maximum values")
            print("Packet Buffers: " + str(averageStats[0]) + "% / "+ str(maxStats[0]))
            print("CPU:            " + str(averageStats[1]) + "% / "+ str(maxStats[1]))
            print("Sessions:       " + str(averageStats[2]) + " / "+ str(maxStats[2]))
            print("CPS:            " + str(averageStats[3]) + " / "+ str(maxStats[3]))
            print("PPS:            " + str(averageStats[4]) + " / "+ str(maxStats[4]))
            print("Kbps:           " + str(averageStats[5]) + " / "+ str(maxStats[5]))


if __name__ == "__main__":
    main(sys.argv[1:])
